package com.meeting.rooms.example.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meeting.rooms.example.model.Employee;
import com.meeting.rooms.example.model.Meeting;
import com.meeting.rooms.example.model.MeetingRoom;
import com.meeting.rooms.example.model.Participant;
import com.meeting.rooms.example.model.dto.EmployeeDto;
import com.meeting.rooms.example.model.dto.MeetingDto;
import com.meeting.rooms.example.model.dto.ParticipantDto;
import com.meeting.rooms.example.payload.request.CreateEmployeeRequest;
import com.meeting.rooms.example.payload.request.CreateEventRequest;
import com.meeting.rooms.example.payload.request.JwtRequest;
import com.meeting.rooms.example.payload.request.UpdateEventRequest;
import com.meeting.rooms.example.repository.EmployeeRepository;
import com.meeting.rooms.example.repository.MeetingRepository;
import com.meeting.rooms.example.repository.MeetingRoomRepository;
import com.meeting.rooms.example.repository.ParticipantRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class ManageMeetingsIntegrationTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MeetingRepository meetingRepository;

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private MeetingRoomRepository meetingRoomRepository;

	@Autowired
	private ParticipantRepository participantRepository;

	@Test
	public void addEmployee_shouldSucceed() throws Exception {

		this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/employee/create" )
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(populateCreateEmployeeRequestDto()))
				.with(csrf()))
				.andExpect(status().isOk());

		Employee employee = employeeRepository.findByName( "Nasko" );
		assertEquals(employee.getName(), "Nasko");

		deleteEmployeeEntries();
	}

	@Test
	public void getAllEmployees_shouldSucceed() throws Exception {

		populateMeetingRoom();
		populateEmployees();

		this.mockMvc.perform( MockMvcRequestBuilders
				.get("/api/employees") )
				.andExpect(status().isOk());

		Collection<Employee> productCollection = employeeRepository.findAll();
		assertNotNull(productCollection);

		deleteEmployeeEntries();
		deleteMeetingRooms();

	}

	@Test
	public void authorization_shouldSucceed() throws Exception{

		populateMeetingRoom();
		populateEmployees();

		String token = this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/authenticate" )
				.contentType( MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(populateJwtRequest())) )
				.andDo(	print())
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		assertNotNull( token );

		deleteEmployeeEntries();
		deleteMeetingRooms();

	}

	@Test
	public void createEvent_shouldSucceed() throws Exception {

		populateMeetingRoom();
		populateEmployees();

		String result = this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/authenticate" )
				.contentType( MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(populateJwtRequest())) )
				.andDo(	print())
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		Map<String, Object> jsonMap = objectMapper.readValue(result,
				new TypeReference<Map<String,Object>>(){});

		String token = jsonMap.get("token").toString();

		this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/event/create" )
				.header("Authorization", "Bearer " + token)
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(populateEventRequest())))
				.andExpect(status().isOk()).andReturn();

		List<Meeting> meetingList = meetingRepository.findAll();

		assertNotNull(meetingList);

		deleteParticipants();
		deleteEvents();
		deleteEmployeeEntries();
		deleteMeetingRooms();
	}

	@Test
	public void createEvent_shouldThrowException() throws Exception {

		this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/event/create" )
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(populateEventRequest())))
				.andExpect(status().is4xxClientError());

	}

	@Test
	public void updateEvent_shouldSucceed() throws Exception {

		populateMeetingRoom();
		populateEmployees();
		Meeting meeting = populateEvent();

		String result = this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/authenticate" )
				.contentType( MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(populateJwtRequest())) )
				.andDo(	print())
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		Map<String, Object> jsonMap = objectMapper.readValue(result,
				new TypeReference<Map<String,Object>>(){});

		String token = jsonMap.get("token").toString();
		Long id = meeting.getId();

		this.mockMvc.perform( MockMvcRequestBuilders
				.put("/api/event/"+ id +"/update" )
				.header("Authorization", "Bearer " + token)
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(populateUpdateEventRequest())))
				.andExpect(status().isOk()).andReturn();

		deleteParticipants();
		deleteEvents();
		deleteEmployeeEntries();
		deleteMeetingRooms();

	}

	@Test
	public void deleteEvent_shouldSucceed() throws Exception {

		populateMeetingRoom();
		populateEmployees();
		Meeting meeting = populateEvent();

		String result = this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/authenticate" )
				.contentType( MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(populateJwtRequest())) )
				.andDo(	print())
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		Map<String, Object> jsonMap = objectMapper.readValue(result,
				new TypeReference<Map<String,Object>>(){});

		String token = jsonMap.get("token").toString();

		Long id = meeting.getId();

		this.mockMvc.perform( MockMvcRequestBuilders
				.delete("/api/event/" + id + "/remove" )
				.header("Authorization", "Bearer " + token)
				.contentType("application/json"))
				.andExpect(status().isOk()).andReturn();

		deleteEmployeeEntries();
		deleteMeetingRooms();
	}

	private CreateEmployeeRequest populateCreateEmployeeRequestDto(){

		EmployeeDto dto1 = new EmployeeDto("Nasko", "nasko", "1234567");
		EmployeeDto dto2 = new EmployeeDto("Rado", "rado", "123456");
		EmployeeDto dto3 = new EmployeeDto("Miro", "miro", "1234");

		List<EmployeeDto> employeeDtos = new ArrayList<>();
		employeeDtos.add(dto1);
		employeeDtos.add(dto2);
		employeeDtos.add(dto3);

		return new CreateEmployeeRequest(employeeDtos);
	}

	private void populateMeetingRoom(){
		MeetingRoom meetingRoom = new MeetingRoom();
		meetingRoom.setName("Tsarevets");
		this.meetingRoomRepository.save(meetingRoom);

		MeetingRoom meetingRoom1 = new MeetingRoom();
		meetingRoom1.setName("Arbanasi");
		this.meetingRoomRepository.save(meetingRoom1);

	}

	private void populateEmployees(){

		Employee employee = new Employee();
		employee.setName("Gosho");
		employee.setUsername("gosho");
		employee.setPassword("$2a$10$lGDzH99XcC5Owxv2TApV1eXpRXAJVEPlfpIeMUSUnhwrjq97vrC02");

		this.employeeRepository.save(employee);

		Employee employee1 = new Employee();
		employee1.setName("Pesho");
		employee1.setUsername("pesho");
		employee1.setPassword("$2a$10$LtLVoS8ykhK69HWESEgr6uuuu4GELHPpMr8BSZXg4nPgrcEM.7.AO");

		this.employeeRepository.save(employee1);

		Employee employee2 = new Employee();
		employee2.setName("Toshko");
		employee2.setUsername("toshko");
		employee2.setPassword("$2a$10$yrnu1FB.xnTwib7zEGQzwOrsa.TihNf.gZSyEsXEC7CK2Gu.EeB3q");

		this.employeeRepository.save(employee2);
	}

	private CreateEventRequest populateEventRequest(){

		List<MeetingDto> meetings = new ArrayList<>();
		MeetingDto dto = new MeetingDto();
		dto.setRoomName("Tsarevets");
		dto.setEnd(new Date());
		dto.setStart(new Date());
		dto.setName("Daily meeting");

		ParticipantDto participantDto = new ParticipantDto();
		participantDto.setUsername("gosho");
		List<ParticipantDto> participantDtos = new ArrayList<>();
		participantDtos.add(participantDto);
		dto.setParticipants(participantDtos);

		meetings.add(dto);

		return new CreateEventRequest(meetings);
	}

	private UpdateEventRequest populateUpdateEventRequest(){
		MeetingDto dto = new MeetingDto();
		dto.setRoomName("Arbanasi");
		dto.setEnd(new Date());
		dto.setStart(new Date());
		dto.setName("Daily meeting");

		ParticipantDto participantDto = new ParticipantDto();
		participantDto.setUsername("gosho");
		ParticipantDto participantDto1 = new ParticipantDto();
		participantDto1.setUsername("pesho");
		List<ParticipantDto> participantDtos = new ArrayList<>();
		participantDtos.add(participantDto);
		participantDtos.add(participantDto1);
		dto.setParticipants(participantDtos);

		return new UpdateEventRequest(dto);
	}

	private JwtRequest populateJwtRequest() {

		JwtRequest request = new JwtRequest();
		request.setUsername("gosho");
		request.setPassword("12345");

		return request;
	}

	private Meeting populateEvent(){
		Meeting meeting = new Meeting();
		meeting.setName("Daily stand up");
		meeting.setStart(new Date());
		meeting.setEnd(generateFakeDate());
		meeting.setMeetingRoom(this.meetingRoomRepository.findByName("Tsarevets").get());

		Participant participant1 = new Participant();
		participant1.setName("Toshko");
		participant1.setUsername("toshko");

		Participant participant2 = new Participant();
		participant2.setName("Gosho");
		participant2.setUsername("Gosho");

		List<Participant> participants = new ArrayList<>();
		participants.add(participant1);
		participants.add(participant2);

		meeting.setParticipants(participants);

		this.meetingRepository.save(meeting);

		return meeting;
	}

	private void deleteEmployeeEntries(){
		this.employeeRepository.deleteAll();
	}

	private void deleteEvents(){
		this.meetingRepository.deleteAll();
	}

	private void deleteMeetingRooms(){
		this.meetingRoomRepository.deleteAll();
	}

	private void deleteParticipants(){ this.participantRepository.deleteAll(); }

	private Date generateFakeDate(){
		Date now = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(now);
		cal.add(Calendar.HOUR, 1);

		return cal.getTime();
	}

}
