package com.meeting.rooms.example.repository;

import com.meeting.rooms.example.model.Meeting;
import com.meeting.rooms.example.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    Optional<Participant> findByUsername(String username);
}
