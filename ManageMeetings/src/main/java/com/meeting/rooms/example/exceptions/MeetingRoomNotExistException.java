package com.meeting.rooms.example.exceptions;

public class MeetingRoomNotExistException extends RuntimeException {

    public MeetingRoomNotExistException(String name) {
        super("Meeting room with name " + name + " does not exist!" );
    }
}
