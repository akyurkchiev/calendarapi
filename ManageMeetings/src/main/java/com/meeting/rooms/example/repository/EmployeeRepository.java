package com.meeting.rooms.example.repository;

import com.meeting.rooms.example.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee findByName(String name );

    Optional<Employee> findByUsername(String username);
}
