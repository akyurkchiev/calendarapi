package com.meeting.rooms.example.exceptions;

public class EmployeeNotFoundException extends RuntimeException {

    public EmployeeNotFoundException(String name) {
        super("Employee with name " + name + " does not exist!" );
    }
}
