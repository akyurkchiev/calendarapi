package com.meeting.rooms.example.payload.request;

import com.meeting.rooms.example.model.dto.EmployeeDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateEmployeeRequest {

    List<EmployeeDto> employees;
}
