package com.meeting.rooms.example.controller;

import com.meeting.rooms.example.model.dto.EmployeeDto;
import com.meeting.rooms.example.exceptions.EmployeeNotFoundException;
import com.meeting.rooms.example.payload.request.CreateEmployeeRequest;
import com.meeting.rooms.example.payload.request.JwtRequest;
import com.meeting.rooms.example.payload.response.JwtResponse;
import com.meeting.rooms.example.security.JwtTokenUtil;
import com.meeting.rooms.example.service.EmployeeService;
import com.meeting.rooms.example.service.JwtUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Slf4j
@RestController
@RequestMapping( "/api" )
public class EmployeeController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping( "/employees")
    public ResponseEntity<Collection<EmployeeDto>> findAll() {
        log.info("Find all existing users. ");
        return ResponseEntity.ok( employeeService.getAll() );
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        log.info("Authentication");
        this.employeeService.authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = jwtUserDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        return ResponseEntity.ok(new JwtResponse( jwtTokenUtil.generateToken(userDetails) ));
    }

    @PostMapping( value = "/employee/create", consumes = {MediaType.APPLICATION_JSON_VALUE} )
    public void create( @RequestBody CreateEmployeeRequest requestDto )
            throws EmployeeNotFoundException {

        log.info("Create new user. ");
        this.employeeService.saveOrUpdate( requestDto );

    }

    @DeleteMapping("/employee/{name}/remove")
    public ResponseEntity<String> remove(@PathVariable("name") String name)
            throws EmployeeNotFoundException {

        this.employeeService.delete( name );

        return new ResponseEntity<>( name, HttpStatus.OK );
    }

}
