package com.meeting.rooms.example.service;

import com.meeting.rooms.example.exceptions.EmployeeNotFoundException;
import com.meeting.rooms.example.exceptions.EventNotFoundException;
import com.meeting.rooms.example.exceptions.MeetingRoomNotExistException;
import com.meeting.rooms.example.model.Employee;
import com.meeting.rooms.example.model.Meeting;
import com.meeting.rooms.example.model.MeetingRoom;
import com.meeting.rooms.example.model.Participant;
import com.meeting.rooms.example.model.dto.MeetingDto;
import com.meeting.rooms.example.model.dto.ParticipantDto;
import com.meeting.rooms.example.payload.request.CreateEventRequest;
import com.meeting.rooms.example.payload.request.UpdateEventRequest;
import com.meeting.rooms.example.payload.response.EventResponse;
import com.meeting.rooms.example.repository.MeetingRepository;
import com.meeting.rooms.example.repository.MeetingRoomRepository;
import com.meeting.rooms.example.repository.EmployeeRepository;
import com.meeting.rooms.example.repository.ParticipantRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class MeetingService {

    @Autowired
    private MeetingRepository meetingRepository;

    @Autowired
    private MeetingRoomRepository meetingRoomRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    public EventResponse getAll(){

        List<Meeting> existingMeetings = meetingRepository.findAll();
        List<MeetingDto> result = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();

        existingMeetings.stream().forEach( meeting -> {
            result.add( modelMapper.map( existingMeetings, MeetingDto.class) );
        });

        return new EventResponse(result);
    }

    public void createEvent(CreateEventRequest request){

        for (MeetingDto dto : request.getMeetings()){
            Optional<MeetingRoom> room = this.meetingRoomRepository.findByName( dto.getRoomName() );

            if (room.isEmpty()){
                throw new MeetingRoomNotExistException( dto.getRoomName() );
            }

            Meeting meeting = new Meeting();
            meeting.setName(dto.getName());
            meeting.setStart(dto.getStart());
            meeting.setEnd(dto.getEnd());
            meeting.setMeetingRoom( room.get() );
            meeting.setParticipants(convertToParticipant(dto.getParticipants()));

            this.meetingRepository.save(meeting);
        }

    }

    public void updateEvent( Long eventId, UpdateEventRequest request ) {

        Optional<Meeting> existingMeeting = this.meetingRepository.findById( eventId );

        if (existingMeeting.isEmpty()){
            throw new EventNotFoundException(eventId);
        }

        Optional<MeetingRoom> room = this.meetingRoomRepository.findByName( request.getEvent().getRoomName() );

        if (room.isEmpty()){
            throw new MeetingRoomNotExistException( request.getEvent().getRoomName() );
        }

        if (existingMeeting.isPresent()){
            Meeting meeting = existingMeeting.get();
            meeting.setName( request.getEvent().getName() );
            meeting.setStart( request.getEvent().getStart() );
            meeting.setEnd( request.getEvent().getEnd() );
            meeting.setMeetingRoom( room.get() );
            meeting.setParticipants( updateParticipants( request.getEvent().getParticipants(), meeting ) );

            this.meetingRepository.save(meeting);

        }

    }

    public void deleteEvent(Long eventId) {

        Optional<Meeting> existingMeeting = this.meetingRepository.findById( eventId );
        if (existingMeeting.isEmpty()){
            throw new EventNotFoundException( eventId );
        }

        this.meetingRepository.delete(existingMeeting.get());
    }

    private Meeting convertMeetingDtoToEntity(MeetingDto dto){

        Optional<MeetingRoom> room = this.meetingRoomRepository.findByName( dto.getRoomName() );

        if ( room.isEmpty() ){
            throw new MeetingRoomNotExistException( dto.getRoomName() );
        }

        Meeting meeting = new Meeting();
        meeting.setName(dto.getName());
        meeting.setStart(dto.getStart());
        meeting.setEnd(dto.getEnd());
        meeting.setMeetingRoom( room.get() );
        meeting.setParticipants(convertToParticipant(dto.getParticipants()));

        this.meetingRepository.save(meeting);

        return meeting;
    }


    private List<Participant> updateParticipants(List<ParticipantDto> participants, Meeting currentMeeting) {

        return checkParticipantAreEmployee(participants, currentMeeting);
    }

    private List<Participant> convertToParticipant(List<ParticipantDto> participantDtos){

        List<Employee> employees = participantDtos.stream().map(participantDto -> {
            Optional<Employee> employee = this.employeeRepository.findByUsername(participantDto.getUsername());
            if (employee.isEmpty()){
                throw new EmployeeNotFoundException( employee.get().getUsername() );
            }
            return employee.get();
        }).collect(Collectors.toList());

        return populateParticipants( employees );
    }

    private List<Participant> populateParticipants(List<Employee> employees) {

        List<Participant> result = new ArrayList<>();

        for (Employee employee : employees){

            Participant participant = new Participant();
            participant.setName(employee.getName());
            participant.setUsername(employee.getUsername());

            this.participantRepository.save(participant);

            result.add(participant);
        }

        return result;
    }

    private List<Participant> populateParticipants(List<Employee> employees, Meeting meeting) {

        List<Participant> result = new ArrayList<>();

        for (Employee employee : employees){

            Optional<Participant> existingParticipant = this.participantRepository.findByUsername( employee.getUsername() );

            if (existingParticipant.isEmpty() ){
                Participant participant = new Participant();
                participant.setName(employee.getName());
                participant.setUsername(employee.getUsername());

                if (!meeting.getParticipants().contains(participant)){
                    List<Meeting> meetings = new ArrayList<>();
                    meetings.add(meeting);
                    participant.setMeetings(meetings);
                }

                this.participantRepository.save(participant);
                result.add(participant);
            }else if (!meeting.getParticipants().contains(existingParticipant.get())){
                List<Meeting> meetings = new ArrayList<>();
                meetings.add(meeting);
                existingParticipant.get().setMeetings(meetings);
                result.add(existingParticipant.get());
            } else {
                result.add(existingParticipant.get());
            }
        }
        return result;
    }

    private List<Participant> checkParticipantAreEmployee( List<ParticipantDto> participantDtos, Meeting currentMeeting ){

        //check if participants are existing employees
        List<Employee> employees = participantDtos.stream().map(participantDto -> {
            Optional<Employee> employee = this.employeeRepository.findByUsername(participantDto.getUsername());
            if (employee.isEmpty()){
                throw new EmployeeNotFoundException( employee.get().getUsername() );
            }
            return employee.get();
        }).collect(Collectors.toList());

        return populateParticipants( employees, currentMeeting );

    }
}
