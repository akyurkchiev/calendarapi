package com.meeting.rooms.example.payload.response;

import com.meeting.rooms.example.model.dto.MeetingDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventResponse {

    private List<MeetingDto> meetingDtos;

}
