package com.meeting.rooms.example.service;

import com.meeting.rooms.example.model.Employee;
import com.meeting.rooms.example.model.dto.EmployeeDto;
import com.meeting.rooms.example.exceptions.EmployeeNotFoundException;
import com.meeting.rooms.example.payload.request.CreateEmployeeRequest;
import com.meeting.rooms.example.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    public Collection<EmployeeDto> getAll(){

        List<Employee> existingEmployees = employeeRepository.findAll();
        List<EmployeeDto> result = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();

        existingEmployees.stream().forEach(meeting -> {
            result.add( modelMapper.map(existingEmployees, EmployeeDto.class) );
        });

        return result;
    }

    public EmployeeDto findByName(String name ) {

        Employee employee = employeeRepository.findByName( name );
        if ( employee == null  ){
            throw new EmployeeNotFoundException( name );
        }

        ModelMapper mapper = new ModelMapper();
        EmployeeDto existingProductDto = new EmployeeDto();

        mapper.map(employee, existingProductDto);

        return existingProductDto;
    }

    public void saveOrUpdate(CreateEmployeeRequest createEmployeeRequest) {

        for (EmployeeDto employeeDto : createEmployeeRequest.getEmployees()){
            if ( employeeRepository.findByName(employeeDto.getName() ) != null){
                Employee existingEmployee = employeeRepository.findByName( employeeDto.getName() );
                existingEmployee.setName( employeeDto.getName() );
                existingEmployee.setPassword(bCryptPasswordEncoder.encode(employeeDto.getPassword()));
                existingEmployee.setUsername(employeeDto.getUsername());
                employeeRepository.save(existingEmployee);
            } else{
                Employee newEmployee = convertDtoToEntity(employeeDto);
                this.employeeRepository.save(newEmployee);
            }
        }
    }

    public void delete(String name) {

        Employee employee = employeeRepository.findByName(name);

        if (ObjectUtils.isEmpty(employee)){
            throw new EmployeeNotFoundException(name);
        }

        employeeRepository.delete(employee);
    }

    public void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }

    }

    private Employee convertDtoToEntity(EmployeeDto requestDto){

        Employee employee = new Employee();
        employee.setName(requestDto.getName());
        employee.setUsername(requestDto.getUsername());
        employee.setPassword(bCryptPasswordEncoder.encode(requestDto.getPassword()));

        return employee;

    }
}
