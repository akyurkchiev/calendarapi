package com.meeting.rooms.example.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table( name = "meeting_rooms")
public class MeetingRoom implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long Id;

    @Column( name = "name", unique = true)
    private String name;

    @OneToMany( fetch = FetchType.EAGER, mappedBy = "meetingRoom" )
    private Set<Meeting> meetings;

}
