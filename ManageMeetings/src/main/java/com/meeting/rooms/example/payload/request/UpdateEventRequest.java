package com.meeting.rooms.example.payload.request;

import com.meeting.rooms.example.model.dto.MeetingDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateEventRequest {

    private MeetingDto event;
}
