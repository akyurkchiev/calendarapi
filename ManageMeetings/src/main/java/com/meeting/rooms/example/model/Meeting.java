package com.meeting.rooms.example.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table( name = "meetings")
public class Meeting implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long Id;

    @Column( name = "subject")
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_at", nullable = false, updatable = false)
    private Date start;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_at", nullable = false, updatable = false)
    private Date end;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn( name = "meeting_room_id", nullable = false)
    private MeetingRoom meetingRoom;

    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.ALL
            })
    @JoinTable(name = "participant_meeting",
            joinColumns = { @JoinColumn(name = "meeting_id") },
            inverseJoinColumns = { @JoinColumn(name = "participant_id") })
    private List<Participant> participants = new ArrayList<>();

}
