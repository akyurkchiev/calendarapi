package com.meeting.rooms.example.exceptions;

public class EventNotFoundException extends RuntimeException {

    public EventNotFoundException(Long id) {
        super("Event with id " + id + " does not exist!" );
    }
}
