package com.meeting.rooms.example.controller;

import com.meeting.rooms.example.exceptions.EventNotFoundException;
import com.meeting.rooms.example.payload.request.CreateEventRequest;
import com.meeting.rooms.example.payload.request.UpdateEventRequest;
import com.meeting.rooms.example.payload.response.EventResponse;
import com.meeting.rooms.example.service.MeetingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping( "/api" )
public class EventController {

    @Autowired
    private MeetingService meetingService;

    @GetMapping( "/events")
    public ResponseEntity<EventResponse> findAll() {
        log.info("Find all existing events. ");
        return ResponseEntity.ok( meetingService.getAll() );
    }

    @PostMapping(value = "/event/create", consumes = {MediaType.APPLICATION_JSON_VALUE} )
    public void create(@RequestBody CreateEventRequest request){

        log.info("Create new meeting event!");

        this.meetingService.createEvent( request );

    }

    @PutMapping(value = "/event/{id}/update", consumes = {MediaType.APPLICATION_JSON_VALUE} )
    public void update(@PathVariable("id") Long eventId, @RequestBody UpdateEventRequest request){

        log.info("Update existing event with id ", eventId);

        this.meetingService.updateEvent( eventId, request );

    }

    @DeleteMapping("/event/{id}/remove")
    public void remove( @PathVariable("id") Long eventId)
            throws EventNotFoundException {

        meetingService.deleteEvent( eventId );

    }

}
