package com.meeting.rooms.example.service;

import com.meeting.rooms.example.model.Employee;
import com.meeting.rooms.example.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Employee employee = employeeRepository.findByUsername( name ).get();
        if (employee == null) {
            log.warn("Authentication error: employee not found with this name: " + name);
            throw new UsernameNotFoundException("Employee not found with this name: " + name);
        }
        return new User(employee.getName(), employee.getPassword() ,new ArrayList<>());
    }
}
